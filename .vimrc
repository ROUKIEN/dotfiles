set nocompatible
filetype off

syn on
set syntax =on
set nu
set nocp
set showmatch
set title

set ai
set smartindent
set cindent
set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2
set cursorline

set rtp +=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'

call vundle#end()
filetype plugin indent on

colorscheme jellybeans
" vuejs file bindings :
autocmd BufNewFile,BufRead *.vue set filetype=html

map <C-n> :NERDTreeTabsToggle<CR>

noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
